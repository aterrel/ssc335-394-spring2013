#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include "mmio.h"
#include "read_mm.h"

/** Forms the LU decomposition of A
 *
 *  A - Matrix to be given LU (input)
 *  L - Lower triangular matrix (output)
 *  U - Upper triangular matrix (output)
 *  M - number of rows and columns (input)
 */
int LU_decomp(double* A, double *L, double *U, int M)
{

  return 0;
}

/** Prints the error ||A - L*U||
 */
int compare_A_to_LU(double *A, double *L, double *U, int M){
  int ndiff = 0;
  double l2_error = 0.0;
  /* ... */
  printf("Error in A - LU, num diff entries: %d, l2 error: %.2e\n", ndiff, sqrt(l2_error));
  return ndiff;
}

/** Forward solve (Ly = b where L = Ux)
 * 
 * L - lower trianguler M X M matrix (input)
 * y - solution vector (MX1) (output)
 * b - right hand side vector (MX1) (input)
 * M - system size (input)
 */
int forward_solve(double* L, double* y, double *b, int M)
{
  return 0;
}

/** Backward solve (Ux = y)
 * 
 * U - backward trianguler M X M matrix (input)
 * x - solution vector (MX1) (output)
 * y - right hand side vector (MX1) (input)
 * M - system size (input)
 */
int backward_solve(double* U, double* x, double *y, int M)
{
  return 0;
}

/** Solves the Ax = b
 *
 *  A - given MXM matrix
 *  x - solution vector (MX1) to output
 *  b - right hand side (MX1) vector
 *  M - size of system
 */
int matrix_solver(double* A, double* x, double* b, int M)
{
  double *L, *U, *y, l2_norm;
  int i;

  L = (double*) malloc(M * M * sizeof(double));
  U = (double*) malloc(M * M * sizeof(double));
  LU_decomp(A, L, U, M);
  y = (double*) malloc(M * sizeof(double));  
  /* ... */
  
  for (i = 0; i < M; ++i) l2_norm += x[i]*x[i];
  printf("L2 norm of solver: %f\n", sqrt(l2_norm));
  free(L);
  free(U);
  free(y);
  return 0;
}



/** Test the LU Routine
 */
int test_matrix_solver(){
  double A[9] = {1, 2, 3, 2, -4, 6, 3, -9, -3};
  double L[9], U[9], x[3];
  double b[3] = {5, 18, 6};
  double error;
  /* ... */
  LU_decomp(A, L, U, 3, 3);
  compare_A_to_LU(/*...*/);
  /* ... */
  matrix_solver(/*...*/);
  /* print comparison to test */
  double sol[3] = {1, -1, 2}
  
  printf("Error in test: %.e\n", error);
  return 0;
}

int main(int argc, char** argv)
{
  int ret_code, M, N, i;
  double *A=NULL, *x, *b;
  FILE *f;

  if (argc < 2) {
    fprintf(stderr, "Usage: %s [martix-market-filename | test]\n", argv[0]);
    return 1;
  }

  if (!strcmp(argv[1], "test")) {
    return test_matrix_solver();
  }

  if ((f = fopen(argv[1], "r")) == NULL) {
    printf("Could not open file.\n");
    return 1;
  }

  ret_code = read_mm(f, &A, &M, &N);
  if (ret_code != 0){
    printf("Error opening Matrix Market file\n");
    return 1;
  } else {
    x = (double*) malloc(M*sizeof(double));
    b = (double*) malloc(M*sizeof(double));
    for (i=0; i<M; ++i) b[i] = i;
    ret_code = matrix_solver(A, x, b, M);
    free(x);
    free(b);
    return ret_code;
  }

  return 0;
}
