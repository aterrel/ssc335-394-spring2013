#ifndef __READ_MM_H
#define __READ_MM_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "mmio.h"


int read_mm(FILE *f, double **A, int *M, int *N);

#endif /* __READ_MM_H */
